<?php


namespace App\Site;


use App\Collection\BasicGallery;
use App\Collection\GalleryProvider;
use App\Site\Fenryll\Fenryll;
use ArrayIterator;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CompanyBasicGalleryProvider extends BasicGallery implements GalleryProvider
{
    private $iterator;

    public function __construct(HttpClientInterface $client)
    {
        parent::__construct('general', 'general', '/');
        $this->addContent((new   Fenryll($client))->makeCollection());
        $this->iterator = new ArrayIterator($this->getContent());
    }

    public function next(): void
    {
        $this->iterator->next();
    }

    public function key(): string
    {
        return $this->current()->getName();
    }

    public function current(): BasicGallery
    {
        return $this->iterator->current();
    }

    public function valid(): bool
    {
        return $this->iterator->valid();
    }

    public function rewind(): void
    {
        $this->iterator->rewind();
    }
}
