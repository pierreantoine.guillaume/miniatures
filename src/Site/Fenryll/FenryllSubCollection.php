<?php


namespace App\Site\Fenryll;


use App\Collection\Miniature;
use App\Collection\RemoteHttpClientGallery;
use Symfony\Component\DomCrawler\Crawler;

class FenryllSubCollection extends RemoteHttpClientGallery
{
    public function fetchRemoteContent(): RemoteHttpClientGallery
    {
        $content = $this->client->request('GET', $this->getPageUrl() . '&n=1000')->getContent();
        $crawler = new Crawler($content);
        $crawler
            ->filter('ul#product_list > li')
            ->each(
                function (Crawler $node) {
                    $a = $node->filter('a');
                    $img = $a->filter('img');
                    $path = preg_replace('#-large#', '', $img->attr('src'));
                    $name = $img->attr('alt');
                    $id = preg_replace('#[^=]+=#', '', $a->attr('href'));
                    $this->addContent(new Miniature($id, $name, $a->attr('href'), $path));
                }
            );
        return $this;
    }
}