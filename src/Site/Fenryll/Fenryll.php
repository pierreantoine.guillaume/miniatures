<?php


namespace App\Site\Fenryll;

use App\Collection\BasicGallery;
use App\Collection\Gallery;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Fenryll
{
    const SITE = "http://fenryll.com/";
    /**
     * @var HttpClientInterface
     */
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @throws ExceptionInterface
     */
    public function makeCollection(string $key = null): Gallery
    {
        $index = $this->fetchIndex();

        $crawler = new Crawler($index);


        $content = new BasicGallery("Fenryll", "Fenryll", self::SITE);
        $client = $this->client;

        $crawler
            ->filter('#categories_block_left > div li > a')
            ->each(
                function (Crawler $node) use ($content, $client, $key) {
                    $link = $node->attr('href');
                    $id = preg_replace("#[^=]+=#", '', $link);
                    $title = html_entity_decode($node->text());

                    $content->addContent($collection = new FenryllSubCollection($id, $title, $link, $client));
                    if ($key === $id) {
                        $collection->fetchRemoteContent();
                    }
                }
            );

        return $content;
    }

    /**
     * @return string
     * @throws ExceptionInterface
     */
    public function fetchIndex(): string
    {
        return $this->client->request('GET', self::SITE)->getContent(true);
    }
}
