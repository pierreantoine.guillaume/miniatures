<?php


namespace App\Command;

use App\Site\Fenryll\Fenryll;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RefreshData extends Command
{
    protected static $defaultName = 'app:refresh-data';

    private $parser;

    public function __construct(Fenryll $parser, string $name = null)
    {
        parent::__construct($name);
        $this->parser = $parser;
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->parser as $url => $name) {
            echo html_entity_decode($name) . " => $url\n";
        }
        return 0;
    }
}