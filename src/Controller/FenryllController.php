<?php


namespace App\Controller;


use App\Site\Fenryll\Fenryll;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class FenryllController extends DefaultController
{
    /**
     * @throws ExceptionInterface
     */
    public function viewGallery(HttpClientInterface $httpClient, string $galleryId): Response
    {
        $indexes = explode('/', $galleryId);
        $fenryll = new Fenryll($httpClient);
        $reference = $fenryll->makeCollection($indexes[0]);
        return $this->renderContent($this->iterateGallery($reference, $indexes));
    }

}