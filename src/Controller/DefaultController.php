<?php


namespace App\Controller;


use App\Collection\Content;
use App\Collection\Gallery;
use App\Collection\GalleryProvider;
use App\Collection\Miniature;
use App\Site\CompanyBasicGalleryProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DefaultController extends AbstractController
{
    public function index(GalleryProvider $provider): Response
    {
        return $this->render("index.html.twig", ['list' => $provider]);
    }

    public function viewGallery(HttpClientInterface $httpClient, string $galleryId): Response
    {
        $indexes = explode('/', $galleryId);
        $reference = new CompanyBasicGalleryProvider($httpClient);
        return $this->renderContent($this->iterateGallery($reference, $indexes));
    }

    protected function renderContent(Content $content):Response
    {
        if ($content instanceof Gallery) {
            return $this->renderGallery($content);
        }
        if ($content instanceof Miniature) {
            return $this->renderMiniature($content);
        }
        throw new HttpException(500, "Could not process request");
    }

    protected function renderGallery(Gallery $reference): Response
    {
        return $this->render('galleryView.html.twig', ['gallery' => $reference]);
    }

    protected function renderMiniature(Miniature $reference): Response
    {
        return $this->render('miniatureView.html.twig', ['miniature' => $reference]);
    }

    protected function iterateGallery(Gallery $reference, array $indexes)
    {
        while ($index = array_shift($indexes)) {
            if (!isset($reference[$index])) {
                throw new NotFoundHttpException(
                    sprintf("No such index %s in gallery : %s", $index, $reference->getName())
                );
            }
            $reference = $reference[$index];
        }
        return $reference;
    }
}