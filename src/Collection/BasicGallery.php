<?php


namespace App\Collection;


use ArrayAccess;

class BasicGallery implements Content, ArrayAccess, Gallery
{
    private $name;
    private $pageUrl;
    private $content;
    private $id;

    public function __construct(string $id, string $name, string $pageUrl)
    {
        $this->id = $id;
        $this->name = $name;
        $this->pageUrl = $pageUrl;
        $this->content = [];
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPageUrl(): string
    {
        return $this->pageUrl;
    }

    public function addContent(Content $content): void
    {
        $this->content[$content->getId()] = $content;
    }

    /**
     * @inheritDoc
     */
    public function getContent(): array
    {
        return $this->content;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function offsetExists($offset): bool
    {
        return isset($this->content[$offset]);
    }

    public function offsetGet($offset): Content
    {
        return $this->content[$offset];
    }

    public function offsetSet($offset, $value): void
    {
        $this->content[$offset] = $value;
    }

    public function offsetUnset($offset): void
    {
        unset($this->content[$offset]);
    }

    public function count(): int
    {
        return count($this->content);
    }
}
