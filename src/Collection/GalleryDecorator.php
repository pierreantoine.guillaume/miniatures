<?php


namespace App\Collection;


abstract class GalleryDecorator implements Gallery
{

    /**
     * @var Gallery
     */
    private $gallery;

    public function __construct(Gallery $gallery)
    {
        $this->gallery = $gallery;
    }

    public function getName(): string
    {
        return $this->gallery->getName();
    }

    public function getPageUrl(): string
    {
        return $this->gallery->getPageUrl();
    }

    public function addContent(Content $content): void
    {
        $this->gallery->addContent($content);
    }

    public function getContent(): array
    {
        return $this->gallery->getContent();
    }

    public function getId(): string
    {
        return $this->gallery->getId();
    }

    public function offsetExists($offset): bool
    {
        return $this->gallery->offsetExists($offset);
    }

    public function offsetGet($offset): Content
    {
        return $this->gallery->offsetGet($offset);
    }

    public function offsetSet($offset, $value): void
    {
        $this->gallery->offsetSet($offset, $value);
    }

    public function offsetUnset($offset): void
    {
        $this->gallery->offsetUnset($offset);
    }

    public function count(): int
    {
        return $this->gallery->count();
    }
}
