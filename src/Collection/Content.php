<?php


namespace App\Collection;


interface Content
{
    public function getName(): string;

    public function getPageUrl(): string;

    public function getId(): string;
}