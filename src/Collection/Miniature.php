<?php


namespace App\Collection;


class Miniature implements Content
{
    private $id;
    private $name;
    private $pageUrl;
    private $image;

    public function __construct(string $id, string $name, string $pageUrl, string $image)
    {
        $this->id = $id;
        $this->name = $name;
        $this->pageUrl = $pageUrl;
        $this->image = $image;
    }


    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPageUrl(): string
    {
        return $this->pageUrl;
    }

    public function getImage(): string
    {
        return $this->image;
    }
}