<?php


namespace App\Collection;


use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class RemoteHttpClientGallery extends BasicGallery
{
    /**
     * @var HttpClientInterface
     */
    protected $client;

    public function __construct(string $id, string $name, string $pageUrl, HttpClientInterface $client)
    {
        parent::__construct($id, $name, $pageUrl);
        $this->client = $client;
    }

    abstract public function fetchRemoteContent(): RemoteHttpClientGallery;
}
