<?php

namespace App\Collection;

use ArrayAccess;
use Countable;

interface Gallery extends Content, ArrayAccess, Countable
{
    public function getName(): string;

    public function getPageUrl(): string;

    public function addContent(Content $content): void;

    /**
     * @return Content[]
     */
    public function getContent(): array;

    public function getId(): string;

    public function offsetExists($offset): bool;

    public function offsetGet($offset): Content;

    public function offsetSet($offset, $value): void;

    public function offsetUnset($offset): void;
}