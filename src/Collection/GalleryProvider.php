<?php


namespace App\Collection;


use ArrayAccess;
use Iterator;

interface GalleryProvider extends Iterator, ArrayAccess
{
    public function current(): BasicGallery;
}